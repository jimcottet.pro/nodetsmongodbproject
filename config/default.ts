export default {
  port: 1337,
  dbUri: "mongodb://localhost:27017/NodeTSProject",
  saltWorkFactor: 10
};